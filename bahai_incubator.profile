<?php
// $Id$

/**
 * @file bahai_incubator.profile
 */

function bahai_incubator_profile_details() {
  return array(
    'name' => 'Incubator Site',
    'description' => 'The local community site incubator is a website setup designed
    to get local communities over the hurdles of hosting their own Drupal website.'
  );
}

function bahai_incubator_profile_modules() {
  $modules = array(
    // Core
    'admin',
    'aggregator',
    'blog',
    'block',
    'color',
    'comment',
    'menu',
    'path',
    'search',
    'taxonomy',
    'update',
    'profile',
    'install_profile_api',

    // Important
    'ctools',
    'features',
    'token',
    'pathauto',
    'path_redirect',
    'globalredirect',
    'custom_breadcrumbs',
    'admin_menu',
    'adminrole',
    'strongarm',
    'userplus',
    'advanced_help',
    'diff',
    'date_api',
    'date_timezone',
    'jquery_ui',
    'context',
    'context_ui',
    'createcontentblock',

    // CCK
    'content',
    'fieldgroup',
    'filefield',
    'filefield_sources',
    'imagefield',
    'number',
    'optionwidgets',
    'text',
    'emfield',
    'emvideo',

    // EMField
    'media_youtube',
    'media_vimeo',
    'media_bliptv',

    // Feeds
    'job_scheduler',
    'feeds',

    // Images
    'imageapi',
    'imageapi_gd',
    'imagecache',
    'imagecache_ui',
    'imagecache_profiles',
    'image_resize_filter',
    'ic_sanity',

    // Location
    'location',
    'location_node',
    'location_user',
    'location_chooser',
    'gmap',
    'gmap_location',
    'gmap_taxonomy',

    // Views
    'views',
    'views_ui',

    // Date stuff
    'date_repeat',
    'date_popup',
    'calendar',
    'calendar_ical',
    'jcalendar',

    // Editing
    'wysiwyg',
    'imce',
    'imce_wysiwyg',
    'better_formats',
    'markdown',
    'typogrify',
    'ed_readmore',
    'submitted_by',

    // Menus
    'accountmenu',

    // Taxonomy
    'taxonomy_super_select',

    // Skinr
    'skinr',
    'vertical_tabs',

    // Features
      // News
      'inc_news',

      // Blog
      'inc_blog',

      // Facilities
      'inc_facilities',
      'inc_localities',

      // Events
      'inc_calendar',

      // Videos
      'inc_video',

      // Pulling everything together at the end
      'inc_base',
      'inc_glue'

  );
  return $modules;
}

function bahai_incubator_profile_task_list() {

}

function bahai_incubator_profile_tasks(&$task, $url) {
  install_include(bahai_incubator_profile_modules());

  $schema = drupal_get_schema(NULL, TRUE);

  features_revert();

  install_profile_field_add(array(
    'title' => 'First Name',
    'name' => 'profile_firstname',
    'type' => 'textfield',
    'category' => 'Bio',
    'weight' => 0,
    'required' => 1,
    'register' => 1,
    'visibility' => 3,
    'autocomplete' => 0,
  ));
  install_profile_field_add(array(
    'title' => 'Last Name',
    'name' => 'profile_lastname',
    'type' => 'textfield',
    'category' => 'Bio',
    'weight' => 0,
    'required' => 1,
    'register' => 1,
    'visibility' => 3,
    'autocomplete' => 0,
  ));
  install_profile_field_add(array(
    'title' => 'Bio',
    'name' => 'profile_bio',
    'type' => 'textarea',
    'category' => 'Bio',
    'weight' => 1,
    'required' => 0,
    'register' => 0,
    'visibility' => 2,
    'autocomplete' => 0,
  ));

  $feeds = array(
    array(
      'title' => "Baha'i World News Service",
      'url' => 'http://news.bahai.org/rss.xml',
      'refresh' => 21600,
    ),
    array(
      'title' => "US Baha'i News",
      'url' => 'http://www.bahai.us/feed/rss/',
      'refresh' => 21600,
    )
  );
  foreach ($feeds as $feed) {
    aggregator_save_feed($feed);
    $feed['fid'] = db_last_insert_id('aggregator_feed', 'fid');
    db_query("INSERT INTO {blocks} (module, theme, delta) VALUES ('%s', '%s', '%s')", 'aggregator', 'feed-' . $feed['fid']);
    aggregator_refresh($feed);
  }

  // Rebuild key tables/caches
  drupal_flush_all_caches();

  #_bahai_incubator_system_theme_data();
  install_disable_theme('garland');
  install_default_theme('enlist');

  install_init_blocks();
  install_disable_block('user', 0, 'enlist');
  install_disable_block('user', 1, 'enlist');
  install_disable_block('system', 0, 'enlist');
  #install_set_block('inc_glue', 'core-activities', 'enlist', 'sidebar_last', -10, 0, "admin/*\ninitialize\nevents\nevents/*");
  #install_set_block('user', 0, 'enlist', 'sidebar_last', 99, 0, "admin/*\ninitialize\nevents\nevents/*");
  #install_set_block('inc_glue', 'invitation', 'enlist', 'sidebar_last', 100, 0, "admin/*\ninitialize\nevents\nevents/*");
  #install_set_block('views', 'Event_List-block_1', 'enlist', 'sidebar_last', 0, 0, "admin/*\ninitialize\nevents\nevents/*");
  #install_set_block('aggregator', 'feed-1', 'enlist', 'splash_first');
  #install_set_block('aggregator', 'feed-2', 'enlist', 'splash_middle');
  #install_set_block('inc_glue', 'principles', 'enlist', 'splash_last');
  #install_set_block('inc_glue', 'contact-link', 'enlist', 'splash_last');
  #install_set_block('views', 'Community_Blogs-block_1', 'enlist', 'sidebar_last', 0, 1, "blogs\nnews");
  #install_set_block('views', 'Community_Blogs-block_2', 'enlist', 'sidebar_last', 0, 1, "users/*");
  install_set_block('search', 0, 'enlist', 'header_top', 0, NULL, NULL, NULL, NULL, '<none>');
  install_set_block('menu', 'accountmenu', 'enlist', 'header_top', 0, NULL, NULL, NULL, NULL, '<none>');

  // FILTERS
  $filters = array(
    array('format' => 1, 'module' => 'filter', 'delta' => 2, 'weight' => '-5'),
    array('format' => 1, 'module' => 'typogrify', 'delta' => 0, 'weight' => '-7'),
    array('format' => 1, 'module' => 'markdown', 'delta' => 0, 'weight' => '-6'),
    array('format' => 1, 'module' => 'filter', 'delta' => 3, 'weight' => '-8'),
    array('format' => 1, 'module' => 'filter', 'delta' => 0, 'weight' => '-9'),
    array('format' => 1, 'module' => 'image_resize_filter', 'delta' => 0, 'weight' => -10),
    array('format' => 2, 'module' => 'image_resize_filter', 'delta' => 0, 'weight' => -10),
    array('format' => 2, 'module' => 'typogrify', 'delta' => 0, 'weight' => '-8'),
    array('format' => 2, 'module' => 'filter', 'delta' => 3, 'weight' => '-9'),
    array('format' => 2, 'module' => 'filter', 'delta' => 2, 'weight' => '-6'),
    array('format' => 2, 'module' => 'markdown', 'delta' => 0, 'weight' => '-7'),
    array('format' => 4, 'module' => 'filter', 'delta' => 3, 'weight' => '-8'),
    array('format' => 4, 'module' => 'filter', 'delta' => 2, 'weight' => '-5'),
    array('format' => 4, 'module' => 'markdown', 'delta' => 0, 'weight' => '-6'),
    array('format' => 4, 'module' => 'typogrify', 'delta' => 0, 'weight' => '-7'),
    array('format' => 4, 'module' => 'filter', 'delta' => 0, 'weight' => '-9'),
  );
  db_query('TRUNCATE TABLE {filters}');
  foreach ($filters as $filter) {
    drupal_write_record('filters', $filter);
  }

  $roles = array_flip(user_roles(TRUE));
  $html_roles = array_intersect_key($roles, array('administrator' => 1, 'editor' => 1, 'reporter' => 1, 'site manager' => 1));
  $html_roles = ',' . implode(',',$html_roles) . ',';
  $formats = array(
    array('format' => 1, 'name' => 'Filtered', 'roles' => ',1,2,', 'weight' => 1),
    array('format' => 2, 'name' => 'Full HTML', 'roles' => $html_roles, 'weight' => 1),
    array('format' => 4, 'name' => 'Comments', 'roles' => ',1,2,', 'weight' => 1),
  );
  db_query("TRUNCATE TABLE {filter_formats}");
  foreach($formats as $format) {
    drupal_write_record('filter_formats', $format);
  }

  $wysiwyg[] = array(
    'format' => 1,
    'editor' => 'tinymce',
    'settings' => 'a:20:{s:7:"default";i:1;s:11:"user_choose";i:1;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:3:{s:7:"default";a:13:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:11:"justifyleft";i:1;s:13:"justifycenter";i:1;s:12:"justifyright";i:1;s:11:"justifyfull";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:4:"link";i:1;s:5:"image";i:1;s:10:"blockquote";i:1;s:12:"removeformat";i:1;}s:5:"paste";a:1:{s:9:"pastetext";i:1;}s:4:"imce";a:1:{s:4:"imce";i:1;}s:6:"drupal";a:1:{s:5:"break";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:0;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:1;s:13:"block_formats";s:32:"p,address,pre,h2,h3,h4,h5,h6,div";s:11:"css_setting";s:5:"theme";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}'
  );
  $wysiwyg[] = array(
    'format' => 2,
    'editor' => 'tinymce',
    'settings' => 'a:20:{s:7:"default";i:1;s:11:"user_choose";i:1;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:3:{s:7:"default";a:13:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:11:"justifyleft";i:1;s:13:"justifycenter";i:1;s:12:"justifyright";i:1;s:11:"justifyfull";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:4:"link";i:1;s:5:"image";i:1;s:10:"blockquote";i:1;s:12:"removeformat";i:1;}s:5:"paste";a:1:{s:9:"pastetext";i:1;}s:4:"imce";a:1:{s:4:"imce";i:1;}s:6:"drupal";a:1:{s:5:"break";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:0;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:0;s:13:"block_formats";s:54:"p,address,pre,h2,h3,h4,h5,h6,div,blockquote,code,dt,dd";s:11:"css_setting";s:5:"theme";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}'
  );
  $wysiwyg[] = array(
    'format' => 4,
    'editor' => 'tinymce',
    'settings' => 'a:20:{s:7:"default";i:0;s:11:"user_choose";i:1;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:2:{s:7:"default";a:7:{s:4:"bold";i:1;s:6:"italic";i:1;s:9:"underline";i:1;s:7:"bullist";i:1;s:7:"numlist";i:1;s:4:"link";i:1;s:12:"removeformat";i:1;}s:5:"paste";a:1:{s:9:"pastetext";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:1;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:0;s:13:"block_formats";s:31:"p,div,blockquote,pre,code,dt,dd";s:11:"css_setting";s:5:"theme";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}'
  );
  db_query("TRUNCATE TABLE {wysiwyg}");
  foreach ($wysiwyg as $w) {
    drupal_write_record('wysiwyg', $w);
  }

  // VARIABLES
  $userplus_roles = array_intersect_key($roles, array('blogger' => 1, 'editor' => 1, 'reporter' => 1, 'host' => 1, 'videographer' => 1));
  foreach ($userplus_roles as $k => $v) {
    $userplus_show_roles[$v] = $v;
  }
  $vars['userplus_add_multiple_roles'] = $vars['userplus_assign_roles'] = $userplus_show_roles;
  $vars['userplus_show_required_profile_fields'] = 1;

  $vars['imce_profiles'] = array(
    1 => array(
      "name" => "Administrators",
      "usertab" => 1,
      "filesize" => "0",
      "quota" => "0",
      "tuquota" => "0",
      "extensions" => "png gif jpg jpeg",
      "dimensions" => "1200x1200",
      "filenum" => "5",
      "directories" => array(
      0 => array(
        "name" => "imce/u%uid",
        "subnav" => 1,
        "browse" => 1,
        "upload" => 1,
        "thumb" => 1,
        "delete" => 1,
        "resize" => 1,
      ),
        1 => array(
          "name" => "imce/common",
          "subnav" => 0,
          "browse" => 1,
          "upload" => 1,
          "thumb" => 1,
          "delete" => 1,
          "resize" => 1,
        ),
      ),
      "thumbnails" => array(
        0 => array(
          "name" => "Thumb",
          "dimensions" => "95x95",
          "prefix" => "",
          "suffix" => "_tn",
          ),
        ),
      ),
    2 => array(
      "name" => "Contributors",
      "usertab" => 1,
      "filesize" => "10",
      "quota" => "0",
      "tuquota" => "0",
      "extensions" => "gif png jpg jpeg",
      "dimensions" => "1200x1200",
      "filenum" => "5",
      "directories" => array(
        0 => array(
          "name" => "imce/u%uid",
          "subnav" => 0,
          "browse" => 1,
          "upload" => 1,
          "thumb" => 1,
          "delete" => 1,
          "resize" => 1,
        ),
        1 => array(
          "name" => "imce/common",
          "subnav" => 0,
          "browse" => 1,
          "upload" => 0,
          "thumb" => 0,
          "delete" => 0,
          "resize" => 0,
        ),
      ),
      "thumbnails" => array(
        0 => array(
          "name" => "Thumb",
          "dimensions" => "95x95",
          "prefix" => "",
          "suffix" => "_tn",
        ),
      ),
    ),
  );
  $imce_roles = array(
    'administrator' => array("weight" => "-10", "pid" => "1", ),
    'site manager' => array("weight" => "-9", "pid" => "1", ),
    'reporter' => array("weight" => "0", "pid" => "2", ),
    'host' => array("weight" => "0", "pid" => "2", ),
    'videographer' => array("weight" => "0", "pid" => "2", ),
    'editor' => array("weight" => "0", "pid" => "2", ),
    'blogger' => array("weight" => "0", "pid" => "2", ),
    'user manager' => array("weight" => "10", "pid" => "0", ),
    'authenticated user' => array("weight" => "11", "pid" => "0", ),
    'anonymous user' => array("weight" => "12", "pid" => "0", ),
  );
  foreach ($roles as $role_name => $rid) {
    $vars['imce_roles_profiles'][$rid] = $imce_roles[$role_name] ? $imce_roles[$role_name] : array('weight' => '15', 'pid' => '0');
  }

  $theme_settings = theme_get_settings();
  $theme_overrides = array(
    'toggle_slogan' => TRUE,
    'toggle_mission' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_node_info_content_feed' => 0,
    'toggle_node_info_page' => 0,
  );
  $vars['theme_settings'] = array_merge($theme_settings, $theme_overrides);

  $vars['location_settings_user'] = array(
    'multiple' => array('min' => '0', 'max' => '1', 'add' => '1'),
    'form' => array('weight' => '9', 'collapsible' => 0, 'collapsed' => 0,
      'fields' => array(
        'name' => array('collect' => '1', 'default' => '', 'weight' => '2'),
        'street' => array('collect' => '1', 'default' => '', 'weight' => '4'),
        'additional' => array('collect' => '1', 'default' => '', 'weight' => '6'),
        'city' => array('collect' => '1', 'default' => '', 'weight' => '8'),
        'province' => array('collect' => '1', 'default' => '', 'weight' => '10'),
        'postal_code' => array('collect' => '1', 'default' => '', 'weight' => '12'),
        'country' => array('collect' => '1', 'default' => 'us', 'weight' => '14'),
        'locpick' => array('collect' => '0', 'weight' => '20'),
      ),
      'register' => 0.
    ),
    'display' => array(
      'weight' => '0',
      'hide' => array(
        'postal_code' => 'postal_code',
        'province' => 'province',
        'country' => 'country',
        'locpick' => 'locpick',
        'province_name' => 'province_name',
        'country_name' => 'country_name',
        'map_link' => 'map_link',
        'province_name' => 'province_name',
        'coords' => 'coords',
        'name' => 0,
        'street' => 0,
        'additional' => 0,
        'city' => 0,
        'province' => 0,
      )
    )
  );

  $vars['preprocess_css'] = '1';
  $vars['preprocess_js'] = '1';
  $vars['block_cache'] = '1';
  $vars['cache'] = '1';

  $vars['site_frontpage'] = 'initialize';
  $vars['site_name'] = "The Baha'i Faith";
  $vars['site_slogan'] = '';
  $vars['admin_theme'] = 'seven';
  $vars['date_first_day'] = '0';
  $vars['gmap_default'] = array(
    'width' => '560px',
    'height' => '400px',
    'latlong' => '44.03626980953461,-123.035888671875',
    'zoom' => '10',
    'maxzoom' => '14',
    'styles' => array(
      'line_default' => array('0000ff', '5', '45', '', ''),
      'poly_default' => array('000000', '3', '25', 'ff0000', '45'),
      'highlight_color' => 'ff0000',
    ),
    'controltype' => 'Small',
    'mtc' => 'standard',
    'maptype' => 'Map',
    'baselayers' => array('Map' => 1, 'Satellite' => 0, 'Hybrid' => 0, 'Physical' => 0),
    'behavior' => array(
      'locpick' => FALSE,
      'nodrag' => FALSE,
      'nokeyboard' => TRUE,
      'nomousezoom' => TRUE,
      'nocontzoom' => FALSE,
      'autozoom' => TRUE,
      'dynmarkers' => FALSE,
      'overview' => FALSE,
      'collapsehack' => FALSE,
      'scale' => FALSE,
      'extramarkerevents' => FALSE,
      'clickableshapes' => FALSE,
      'googlebar' => FALSE,
      'highlight' => FALSE,
    ),
    'markermode' => '0',
    'linecolors' => array(
      '#00cc00', '#ff0000','#0000ff',
    )
  );
  $vars['gmap_location_block_macro'] = '[gmap |width=100% |height=240px |control=None |behavior=+autozoom +notype]';
  $vars['location_usegmap'] = 1;
  $opts = variable_get('gmap_markermanager', array());
  $opts['clusterer'] = array(
            'filename' => 'Clusterer2.js/Clusterer2.js',
            'marker' => 'cluster',
            'max_nocluster' => 100,
            'cluster_min' => 10,
            'max_lines' => 10,
            'popup_mode' => 'orig',
  );
  $vars['gmap_markermanager'] = $opts;
  $vars['gmap_markermanager_clusterer_filename'] = 'Clusterer2.js/Clusterer2.js';
  $vars['gmap_mm_type'] = 'clusterer';
  $vars['gmap_markerfiles'] = drupal_get_path('profile', 'bahai_incubator') . '/libraries/gmap-markers';
  $vars['location_geocode_us'] = 'google';
  $vars['location_geocode_google_minimum_accuracy'] = '3';
  $vars['location_general_geocoders_in_use'] = array('google' => 'google');
  $vars['user_pictures'] = '1';
  $vars['user_picture_path'] = 'images/users';
  $vars['user_picture_default'] = '';
  $vars['skinr_enlist'] = array(
    'block' => array(
      'inc_glue-invitation' => array('enlist-custom-block-styles' => array('enlist-block-no-background' => 'enlist-block-no-background')),
      'inc_glue-core-activities' => array('enlist-custom-block-styles' => array('enlist-block-big-list' => 'enlist-block-big-list')),
      'user-0' => array('fusion-login' => 'fusion-horiz-login', 'enlist-custom-block-styles' => array('enlist-block-no-background' => 'enlist-block-no-background')),
      'menu-accountmenu' => array('fusion-menu' => 'fusion-inline-menu', 'enlist-custom-block-styles' => array('enlist-block-white-links' => 'enlist-block-white-links')),
    ),
  );
  foreach($vars as $key => $value) {
    variable_set($key, $value);
  }
  gmap_get_icondata();
  gmap_flush_caches();

  if (module_exists('inc_glue')) {
    include_once(drupal_get_path('module', 'inc_glue') . '/inc_glue.admin.inc');
    inc_glue_set_L10n();

    // GET DEFAULT CONTENT
    // I'm leaving the following lines in place just in case feeds decides to act up again...
    #include_once(drupal_get_path('module', 'inc_base') . '/inc_base.feeds_importer_default.inc');
    #$importer = inc_base_feeds_importer_default();
    #db_query("INSERT INTO {feeds_importer} (id, config) VALUES ('content_feed', '%s')", serialize($importer['content_feed']));
    $feedurl['FeedsHTTPFetcher']['source'] = 'http://labs.uswebdev.net/feeds/core-activities/feed';
    // Save source and import.
    $source = feeds_source('content_feed');
    $source->addConfig($feedurl);
    $source->save();

    // Add to schedule, make sure importer is scheduled, too.
    $source->schedule();
    $source->importer->schedule();
    db_query("UPDATE {job_schedule} SET next = %d WHERE callback = 'feeds_source_import' AND type = 'content_feed'", time());

  }

}

function bahai_incubator_form_alter(&$form, $form_state, $form_id) {

}

/**
 * Reimplementation of system_theme_data(). The core function's static cache
 * is populated during install prior to active install profile awareness.
 * This workaround makes enabling themes in profiles/[profile]/themes possible.
 */
function _bahai_incubator_system_theme_data() {
  global $profile;
  $profile = 'openatrium';

  $themes = drupal_system_listing('\.info$', 'themes');
  $engines = drupal_system_listing('\.engine$', 'themes/engines');

  $defaults = system_theme_default();

  $sub_themes = array();
  foreach ($themes as $key => $theme) {
    $themes[$key]->info = drupal_parse_info_file($theme->filename) + $defaults;

    if (!empty($themes[$key]->info['base theme'])) {
      $sub_themes[] = $key;
    }

    $engine = $themes[$key]->info['engine'];
    if (isset($engines[$engine])) {
      $themes[$key]->owner = $engines[$engine]->filename;
      $themes[$key]->prefix = $engines[$engine]->name;
      $themes[$key]->template = TRUE;
    }

    // Give the stylesheets proper path information.
    $pathed_stylesheets = array();
    foreach ($themes[$key]->info['stylesheets'] as $media => $stylesheets) {
      foreach ($stylesheets as $stylesheet) {
        $pathed_stylesheets[$media][$stylesheet] = dirname($themes[$key]->filename) .'/'. $stylesheet;
      }
    }
    $themes[$key]->info['stylesheets'] = $pathed_stylesheets;

    // Give the scripts proper path information.
    $scripts = array();
    foreach ($themes[$key]->info['scripts'] as $script) {
      $scripts[$script] = dirname($themes[$key]->filename) .'/'. $script;
    }
    $themes[$key]->info['scripts'] = $scripts;

    // Give the screenshot proper path information.
    if (!empty($themes[$key]->info['screenshot'])) {
      $themes[$key]->info['screenshot'] = dirname($themes[$key]->filename) .'/'. $themes[$key]->info['screenshot'];
    }
  }

  foreach ($sub_themes as $key) {
    $themes[$key]->base_themes = system_find_base_themes($themes, $key);
    // Don't proceed if there was a problem with the root base theme.
    if (!current($themes[$key]->base_themes)) {
      continue;
    }
    $base_key = key($themes[$key]->base_themes);
    foreach (array_keys($themes[$key]->base_themes) as $base_theme) {
      $themes[$base_theme]->sub_themes[$key] = $themes[$key]->info['name'];
    }
    // Copy the 'owner' and 'engine' over if the top level theme uses a
    // theme engine.
    if (isset($themes[$base_key]->owner)) {
      if (isset($themes[$base_key]->info['engine'])) {
        $themes[$key]->info['engine'] = $themes[$base_key]->info['engine'];
        $themes[$key]->owner = $themes[$base_key]->owner;
        $themes[$key]->prefix = $themes[$base_key]->prefix;
      }
      else {
        $themes[$key]->prefix = $key;
      }
    }
  }

  // Extract current files from database.
  system_get_files_database($themes, 'theme');
  db_query("DELETE FROM {system} WHERE type = 'theme'");
  foreach ($themes as $theme) {
    $theme->owner = !isset($theme->owner) ? '' : $theme->owner;
    db_query("INSERT INTO {system} (name, owner, info, type, filename, status, throttle, bootstrap) VALUES ('%s', '%s', '%s', '%s', '%s', %d, %d, %d)", $theme->name, $theme->owner, serialize($theme->info), 'theme', $theme->filename, isset($theme->status) ? $theme->status : 0, 0, 0);
  }
}
