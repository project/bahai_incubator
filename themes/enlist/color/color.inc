<?php

$info = array(

  'schemes' => array(
	// base, link, gradation(top), gradation(bottom), text
    '#1977aa,#ba4308,#cecece,#f0f0f0,#252525' => "Standard (default)",
    '#2e6b60,#265aab,#95c6b6,#6eb2bf,#252525' => "Deep Marine",
    '#25a26c,#a42823,#9ee0ac,#92bfc4,#1f3d2d' => "Shallow Marine",
    '#3a6944,#b65216,#f0dfcc,#a4865b,#301503' => "Flagstaff",
  ),

  'copy' => array(
    'images/logo.png',
    'images/nav-grey.png',
    'images/block-grey.png',
  ),

  'css' => array(
    'css/enlist-style.css',
    'css/enlist-style-rtl.css',
    'css/local.css',
    'css/local-rtl.css'
  ),

  'gradient' => array(0,92,800,167),

  'fill' => array(
    'base' => array(0,0,800,600),
    'link' => array(194,332,131,20),
    'text' => array(53,332,141,20),
  ),

  'slices' => array(
    'images/nav-color.png' => array(9,92,8,37),
    'images/block-color.png' => array(525,159,8,115),
    'images/bg-color.png' => array(0,0,8,8),
  ),
  
  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation
  'base_image' => 'color/base.png',

);