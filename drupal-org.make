core = 6.22

projects[custom_breadcrumbs] = 2.0-rc1

projects[imce] = 2.1

projects[location] = 3.1

projects[seven] = 1.0-rc1

projects[ctools] = 1.8

projects[token] = 1.16

projects[pathauto] = 1.5

projects[path_redirect] = 1.0-rc2

projects[admin_menu] = 1.6

projects[adminrole] = 1.3

projects[sudo] = 1.1

projects[css_injector] = 1.4

projects[userplus] = 2.5

projects[vertical_tabs] = 1.0-rc1

projects[advanced_help] = 1.2

projects[install_profile_api] = 2.1

projects[features] = 1.0

projects[strongarm] = 2.0

projects[diff] = 2.1

projects[context] = 3.0

projects[cck] = 2.9

projects[link] = 2.9

projects[filefield] = 3.10

projects[imagefield] = 3.10

projects[emfield] = 2.5

projects[email] = 1.2

projects[filefield_sources] = 1.4

projects[image_resize_filter] = 1.12

projects[webform] = 3.11

projects[flag] = 1.3

projects[imce_wysiwyg] = 1.1

projects[media_youtube] = 1.3

projects[media_vimeo] = 1.1

projects[media_bliptv] = 1.0-beta1

projects[job_scheduler] = 1.0-beta3

projects[feeds] = 1.0-beta10

projects[location_chooser] = 1.1

projects[date] = 2.7

projects[jquery_ui] = 1.4

projects[views] = 2.12

projects[views_slideshow] = 2.3

projects[imageapi] = 1.10

projects[imagecache] = 2.0-beta12

projects[imagecache_profiles] = 1.3

projects[wysiwyg] = 2.3

projects[better_formats] = 1.2

projects[markdown] = 1.2

projects[typogrify] = 1.0

projects[ed_readmore] = 5.0-rc7

projects[realname] = 1.4

projects[accountmenu] = 1.8

projects[taxonomy_super_select] = 1.4

projects[skinr] = 1.6

projects[fusion] = 1.1


