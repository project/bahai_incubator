// $Id$

The Local Baha'i Community Website Incubator is a Drupal installation profile
incorporating many features that would be commonly used on a website for a local
Baha'i community or a group of communities, including news, events, blogs, 
facilities and localities, map and social media integration, and user-submitted 
content.


INSTALLATION AND SETUP
======================

Follow the instructions in the INSTALL.txt file.


USAGE AND EXTENSION
===================

Once you complete the installation and setup, your website should function much
the same as any other Drupal website, and for the most part you should be able
to configure it as you see fit.  There are a few areas of the site, related to
the content types, where the configuration is bundled up as "features" (see
http://drupal.org/project/features for more information), and you can turn these
features on or off under Site building -> Features (admin/build/features).

You can add modules or themes to the site as you would any other Drupal site,
keeping in mind that if you wish to upgrade your Incubator installation later,
you may have to keep track of what you added.


UPGRADING
=========

This section will be added as soon as there is an upgrade available.


HELP AND SUPPORT
================

In this early release, there is not much specific help for the Incubator.  If
you run into problems, we suggest the following course of action:

1. Try to search for your problem on google or on drupal.org; in many cases,
   problems of a general nature have already been solved by others.

2. Search the issues at http://drupal.org/project/issues/bahai_incubator (the
   issue queue for the Incubator project) and see if the problem is mentioned.

3. If you can't find your problem in the issue queue, please post a new issue.
   This will help us to improve the Incubator for everyone.
