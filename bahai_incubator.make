; $Id$

core = 6.x
api = 2

projects[] = ctools
projects[] = token
projects[] = pathauto
projects[] = path_redirect
;projects[global_redirect] = 1.x-dev                                            ;cannot use dev versions
projects[] = admin_menu
;projects[admin][patch][] = "http://drupal.org/files/issues/play_nice_with_admin_menu.patch"  ;cannot use patches
projects[] = adminrole
projects[] = sudo
projects[] = css_injector
projects[] = userplus
projects[] = vertical_tabs
projects[] = advanced_help
projects[] = install_profile_api

projects[] = features
projects[] = strongarm
projects[] = diff
projects[] = context

projects[] = cck
projects[] = link
projects[] = filefield
projects[] = imagefield
projects[] = emfield
projects[] = email
projects[] = filefield_sources
projects[custom_breadcrumbs] = 2.0-rc1
projects[] = image_resize_filter
projects[] = webform
projects[] = flag
projects[imce] = 2.1
projects[] = imce_wysiwyg

projects[] = media_youtube
projects[] = media_vimeo
projects[] = media_bliptv

projects[] = job_scheduler
projects[] = feeds

projects[location] = 3.1
projects[] = location_chooser
;projects[gmap][version] = 1.1
;projects[gmap][patch][] = "http://drupal.org/files/issues/703480_return_behaviors.patch" ;cannot use patches

projects[] = date
;projects[calendar][version] = 2.4
;projects[calendar][patch][] = "http://drupal.org/files/issues/679496_css_classes_per_day.patch" ;cannot use patches
projects[] = jquery_ui

projects[] = views
projects[] = views_slideshow

projects[] = imageapi
projects[] = imagecache
projects[] = imagecache_profiles

projects[] = wysiwyg
projects[] = better_formats
projects[] = markdown
projects[] = typogrify
projects[] = ed_readmore
projects[] = realname
projects[] = accountmenu

;projects[submitted_by] = 1.x-dev                                               ;cannot use dev versions

projects[] = taxonomy_super_select

projects[] = skinr
projects[] = fusion
projects[seven][type] = theme

; Libraries

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://downloads.sourceforge.net/project/tinymce/TinyMCE/3.3.7/tinymce_3_3_7.zip"
libraries[tinymce][directory_name] = "tinymce"

libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"
libraries[jquery_ui][directory_name] = "jquery.ui"
libraries[jquery_ui][destination] = "modules/jquery_ui"

libraries[Clusterer2.js][download][type] = "file"
libraries[Clusterer2.js][download][url] = "http://acme.com/javascript/Clusterer2.js"
libraries[Clusterer2.js][destination] = "modules/gmap/thirdparty"
libraries[Clusterer2.js][filename] = "Clusterer2.js"

libraries[i18n-ascii][download][type] = "file"
libraries[i18n-ascii][download][url] = "http://drupalcode.org/project/pathauto.git/blob_plain/6395f3187dec5c7fa84d79117518c13449a21a4b:/i18n-ascii.example.txt"
libraries[i18n-ascii][destination] = "sites"
libraries[i18n-ascii][directory_name] = "all"
libraries[i18n-ascii][filename] = "i18n-ascii.txt"
