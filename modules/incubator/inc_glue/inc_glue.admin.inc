<?php
// $Id$

/**
 * @file
 * Provides admin forms for the site incubator
 */

function inc_glue_welcome_page() {
  drupal_set_title(t('Welcome!'));
  $loc = variable_get('inc_glue_L10n', FALSE);
  if ($loc) {
    $output = $loc->welcome;
  }
  else {
    $output = "<p>This site is under construction. To find out more about the Bahá'í Faith, please check out one of the following official Bahá'í websites:</p>
    <h3><a href='http://bahai.org'>Bahá'í Faith - international website of the Bahá'ís of the world</a></h3>
    <p>Bahai.org is the official Bahá'í website. It has links to Bahá'í writings, articles about the life and teachings of Bahá'u'lláh, news, media, and links to Bahá'í communities around the world.</p>";
  }
  return t($output);
}

/**
 *
 * @return array
 * Returns a keyed array of menu items to replicate, in a keyed array of vertical tabs.
 * The array goes like this:<code>
 * $items['vertical_tab_machine_name'] = array(
 *   'field' = (an array of #title and #weight for the fieldset)
 *   array(
 *     'module' => 'the_module_to_take_from',
 *     'file' => 'the_file_to_take_from',
 *     'function' => 'the_form_definition_function',
 *     'fields' => array(
 *       'first_field' => true,
 *       'second_field' => array('field' => array('#title' => t('New Title'))),
 *     ),
 *   ),
 * );
 */
function _inc_glue_menu_items() {
  $items['info'] = array(
    'field' => array(
      '#title' => t('Site Information'),
      '#weight' => -100,
    ),
    array(
      'module' => 'inc_glue',
      'file' => 'inc_glue.admin.inc',
      'function' => 'inc_glue_settings_form',
      'fields' => array(
        'inc_glue_L10n' => TRUE,
      )
    ),
    array(
      'module' => 'system',
      'file' => 'system.admin.inc',
      'function' => 'system_site_information_settings',
      'fields' => array(
        'site_name' => array('field' => array(
          '#title' => t('Site Title'),
          '#weight' => -50,
          '#description' => t("The name of this website.") .
            '<br/>' . t("Examples: \"The Baha'i Faith\" <i>(with subtitle)</i>, \"The Baha'i Faith in Eugene Oregon\", \"Eugene Baha'i Faith\", \"Baha'is of Eugene Oregon\"")
        )),
        'site_slogan' => array('field' => array(
          '#title' => t('Site Subtitle'),
          '#weight' => -49,
          '#description' => t("Your site's motto, tag line, or catchphrase (often displayed alongside the title of the site).") . 
            '<br/>' . t('Examples: "for the Eugene metro area", "Including Eugene, Springfield, Cottage Grove, and Lane County"'),
        )),
        'site_mail' => TRUE,
        'site_footer' => TRUE,
        'site_frontpage' => TRUE,
      )
    ),
  );
  $items['date_and_time'] = array(
    'field' => array(
      '#title' => t('Date and Time'),
      '#weight' => -30,
    ),
    array(
      'module' => 'system',
      'file' => 'system.admin.inc',
      'function' => 'system_date_time_settings',
      'fields' => array(
        'locale' => array(
          'date_default_timezone' => TRUE,
          'configurable_timezones' => TRUE,
        ),
        'date_formats' => array(
          'date_format_short' => TRUE,
          'date_format_short_custom' => TRUE,
          'date_format_medium' => TRUE,
          'date_format_medium_custom' => TRUE,
          'date_format_long' => TRUE,
          'date_format_long_custom' => TRUE,
        )
      )
    )
  );
  $items['incubator_user_registration'] = array(
    'field' => array(
      '#title' => t('User Registration'),
      '#weight' => -25,
    ),
    array(
      'module' => 'user',
      'file' => 'user.admin.inc',
      'function' => 'user_admin_settings',
      'fields' => array(
        'registration' => array(
          'user_register' => TRUE,
          'user_email_verification' => TRUE,
        )
      )
    )
  );
  $items['incubator_location'] = array(
    'field' => array(
      '#title' => t('Location & Mapping'),
      '#weight' => -20,
    ),
    'Google Map Settings' => array(
      'module' => 'gmap',
      'file' => 'gmap_settings_ui.inc',
      'function' => 'gmap_admin_settings',
      'fields' => array(
        'initialization' => array(
          'googlemap_api_key' => array('field' => array(
            '#description' => t('<span class="red">Important:</span> To use maps, your site needs a Googlemaps API key.  You must get this for each separate website at the <a href="http://www.google.com/apis/maps/signup.html" target="_blank">Google Map API website</a> - visit that page, fill out the form, copy the key, and paste it into this box.'),
          )),
        ),
      ),
    ),
    array(
      'module' => 'location_chooser',
      'file' => 'location_chooser.admin.inc',
      'function' => 'location_chooser_location_form',
      'fields' => array(
        'default_location' => TRUE,
      )
    )
  );
  return $items;
}

function inc_glue_admin_form() {

  if (!user_access('administer site configuration')) {
    drupal_goto(variable_get('site_frontpage', 'initialize') == 'initialize' ? 'welcome' : '');
  }

  if (!variable_get('incubator_theme_initialize', FALSE)) {
    $theme_key = variable_get('theme_default', 'enlist');
    db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' AND name = '%s'", $theme_key);
    system_theme_data();
    menu_rebuild();
    cache_clear_all();
    variable_set('incubator_theme_initialize', TRUE);
  }
  $menu_items = _inc_glue_menu_items();

  // FIELDSETS
  foreach ($menu_items as $fieldset => $items) {

    // initialize the fieldset
    $form[$fieldset] = array(
      '#type' => 'fieldset',
      '#group' => $fieldset,
    ) + $items['field'];
    unset ($items['field']);

    foreach ($items as $name => $keys) {

      // add all the fields
      $path = drupal_get_path('module', $keys['module']);
      include_once($path . '/' . $keys['file']);
      $ff = call_user_func($keys['function'], array());
      $elements = _inc_glue_form_array($ff, $keys['fields']);
      if (is_array($ff)) {
        if (!is_numeric($name)) {
          $machine = preg_replace('/^[_a-z0-9]/', '', str_replace(' ', '_', $name));
          $form[$fieldset][$machine] = array('#type' => 'fieldset','#title' => t($name)) + $elements;
        }
        else {
          $form[$fieldset] += $elements;
        }
      }
    }
  }

  // FINISH THE FORM
  $form['intro'] = array(
    '#type' => 'markup',
    '#value' => '<h2>' . t('Incubator Setup Wizard') . '</h2><p>' .
                t('The basic settings below should get your new website up and running.  Once you become familiar with the administrative areas of the website,
                  available through the black menu bar at the top of the page, you will want to change the default front page away from "initialize", which you
                  can do under "Site configuration -> Site information".  First, however, you should enter the settings in each of the tabs below.') . '</p>',
    '#weight' => -1000,
  );
  $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  $form = system_settings_form($form);
  $form['#submit'][] = 'inc_glue_admin_form_submit';
  $form['#submit'][] = 'inc_glue_settings_form_submit';
  $form['#submit'] = array_reverse($form['#submit']);
  drupal_add_css(drupal_get_path('module', 'inc_glue') . '/css/inc_glue_admin_form.css');
  if (is_array($form['date_and_time']) && module_exists('date_timezone')) {
    date_timezone_site_form($form['date_and_time']);
    $form['date_and_time']['#after_build'][] = 'date_timezone_site_form_after_build';
  }
  return $form;
}

function inc_glue_admin_form_submit(&$form, &$form_state) {
  location_chooser_location_form_submit($form, $form_state);
  unset($form_state['values']['default_location']);
}

function _inc_glue_form_array($master, $mask) {
  if (!is_array($master)) { return $master; }
  foreach ($master as $k=>$v) {
    if (!isset($mask[$k])) { unset ($master[$k]); continue; } // remove value from $master if the key is not present in $mask
    if (is_array($mask[$k]['field'])) { // merge in the field values
      $master[$k] = array_merge($master[$k], $mask[$k]['field']);
      unset($mask[$k]['field']);
    }
    if (is_array($mask[$k]) && !empty($mask[$k])) { // recurse when mask is still an array
      $master[$k] = _inc_glue_form_array($master[$k], $mask[$k]);
    }
    // else simply keep value
  }
  return $master;
}

function inc_glue_settings_form() {
  $loc = variable_get('inc_glue_L10n', FALSE);
  $files = array_keys(file_scan_directory(drupal_get_path('module', 'inc_glue') . '/L10n', '\.inc$', array('.', '..', 'CVS'), 0, FALSE));
  foreach ($files as $filename) {
    $filename = preg_replace('#^.*/#', '', $filename);
    $options[$filename] = str_replace(array('-', '_', '.inc'), ' ', "$filename.inc");
  }
  array_unshift($options, '- none -');
  $form['inc_glue_L10n'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Regional content'),
    '#default_value' => $loc ? $loc->file : 0,
    '#description' => t('Choose a region above to get suitable content blocks and pages'),
  );
  $form['#submit'][] = 'inc_glue_settings_form_submit';
  return system_settings_form($form);
}

function inc_glue_settings_form_submit(&$form, &$form_state) {
  inc_glue_set_L10n($form_state['values']['inc_glue_L10n']);
  unset($form_state['values']['inc_glue_L10n']);
}

function inc_glue_set_L10n($value = "United-States.inc") {
  if (file_exists($file = drupal_get_path('module', 'inc_glue') . '/L10n/' . $value)) {
    include_once($file);
    $loc['welcome'] = $welcome;
    $loc['blocks'] = $blocks;
    $loc['file'] = $value;
    variable_set('inc_glue_L10n', (object) $loc);
  }
  else {
    variable_del('inc_glue_L10n');
  }
}