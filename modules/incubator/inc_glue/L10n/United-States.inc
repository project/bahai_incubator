<?php
// $Id$

/**
 * @file
 *
 * This is a localization file for Baha'i communities to use in the incubator.
 * It is php code that will be included in page loads, so only the system
 * administrator should be allowed to change it.
 *
 * WELCOME =====================================================================
 * The $welcome variable is the text that will display by default on the front
 * page of new sites.  Full HTML may be included.
 *
 * BLOCKS ======================================================================
 * Blocks are defined in array format as indicated below:
 * 'title' => 'The Name on the Administrative Interface',
 * 'subject' => 'The Block Title on Viewed Pages',
 * 'content' => 'the content for the block, whatever it may be.',
 *
 * Each of these strings is translatable.
 *
 * The system name of each block appears between the [brackets].
 * The default system names are 'principles', 'invitation', 'contact-form', and
 * 'contact-link'.  You can give blocks any system name you choose, or simply
 * leave an empty bracket; however, if you choose names other than the defaults,
 * you will have to configure the blocks to appear on your pages either through
 * the block administration page or through the contexts.
 *
 */

$welcome = "<p>This site is under construction. To find out more about the Bahá'í Faith, please check out one of the following official Bahá'í websites:</p>
<h3><a href='http://bahai.org'>Bahá'í Faith - international website of the Bahá'ís of the world</a></h3>
<p>Bahai.org is the official Bahá'í website. It has links to Bahá'í writings, articles about the life and teachings of Bahá'u'lláh, news, media, and links to Bahá'í communities around the world.</p>
<h3><a href='http://www.bahai.us'>Bahá'í Faith - United States website</a></h3>
<p>The official website of the Bahá'ís of the United States has news, publications and media which would be of interest to residents of the United States, including tools to <a href='http://find.bahai.us'>find Bahá'í communities near you</a> or to <a href='http://contact.bahai.us'>contact the Bahá'ís</a>.</p>";

$blocks['principles'] = array(
  'title' => "Baha'i Principles",
  'subject' => "Baha'i Principles",
  'content' => '<ul>
  <li><a href="http://info.bahai.org/article-1-3-2-17.html">Independent investigation of truth</a></li>
  <li><a href="http://www.bahai.us/racism-in-america">Elimination of racial discrimination</a></li>
  <li><a href="http://info.bahai.org/article-1-8-0-4.html">Equality of women and men</a></li>
  <li><a href="http://info.bahai.org/article-1-3-2-18.html">Harmony of science and religion</a></li>
  <li><a href="http://bic.org/statements-and-reports/bic-statements/08-0214.htm">Economic justice</a></li>
  <li><a href="http://info.bahai.org/pdf/letter_april2002_english.html">Unity of religion</a></li>
  <li><a href="http://info.bahai.org/article-1-3-2-16.html">Universal education</a></li>
  </ul>'
);
$blocks['invitation'] = array(
  'title' => 'Join Link',
  'content' => '<div style="text-align:center;"><a href="https://join.bahai.us/Invitation.aspx" target="_blank"><img height="116" border="0" align="bottom" width="213" style="margin:0 auto;" src="http://www.bahai.us/files/iwanttobeaBahai.gif" alt=""></a></div>',
);
$blocks['contact-form'] = array(
  'title' => 'Contact Form',
  'content' =>
    '<script type="text/javascript">var host = (("https:" == document.location.protocol) ? "https://secure." : "http://");document.write(unescape("%3Cscript src=\'" + host + "wufoo.com/scripts/embed/form.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>
    <script type="text/javascript">
    var k7x2w5 = new WufooForm();
    k7x2w5.initialize({
    \'userName\':\'bahai\',
    \'formHash\':\'k7x2w5\',
    \'autoResize\':true,
    \'height\':\'903\',
    \'ssl\':true});
    k7x2w5.display();
    </script>
    '
  );
$blocks['contact-link'] = array(
  'title' => 'Contact Link',
  'subject' => 'Contact Us',
  'content' => t("Contact the Baha'is through the official website at " . l('http://contact.bahai.us', 'http://contact.bahai.us/RequestInformation.aspx', array('external' => TRUE))),
);

// plesae DON'T close the <?php tag, and DO leave one blank line at the end of the file
