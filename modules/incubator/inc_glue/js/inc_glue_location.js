Drupal.behaviors.incGlueLocation = function(context) {
  $("#edit-location-chooser").change(function() {
    var newLocation = $(this).val();
    var newLocationData = Drupal.settings.inc_glue_location[newLocation];
    $.each(newLocationData, function(key, value) {
      $("#edit-locations-0-" + key).val(value);
    })
  })
  $("fieldset.location .form-item input").change(function() {
    $("#edit-location-chooser").val(Drupal.settings.inc_glue_location.def);
  })
}
